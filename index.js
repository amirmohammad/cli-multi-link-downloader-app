#!/usr/bin/env node
const fs = require('fs')
const request = require('request')


const file = process.argv[2]
if(!file) {
  console.log('enter file path')
  process.exit(0)
}

const lines = fs.readFileSync(file, 'utf-8').split(/\r?\n/).filter(link => link != '')

const download = (url, filename, callback) => {
  request.get({
    url, 
  })
  .on('error', (err) => console.log(`can't download ${url}`, err))
  .on('response', (res) =>{
    res.pipe(fs.createWriteStream(`${filename}.` + res.headers['content-type'].split('/')[1]));
  })
  .on('close', callback)
}

for(line in lines) {
    let url = lines[line]
    let filename = `${line}`

    download(url, filename, () => {
        console.log('✅ Done!')
    })

}